import Newapi from './api';

import aeCodes from './apiErrorCodes';
// import * as Storage from '../Storage';
// import { Platform } from 'react-native';
// import DeviceInfo from 'react-native-device-info';
import { decoding } from '../Utils';

const ApiCall = Newapi.getInstance();

export function isCanceled(err) {
  return err.isCanceled;
}

export function isWSError(err) {
  switch (err.code) {
    case aeCodes.WS_CONNECT_ERROR.code:
    case aeCodes.WS_ERROR.code:
      return true;
    default:
      return false;
  }
}

export function getAllMangaList() {
  return ApiCall.wsCall('/list/0/');
}
export function getMangaList(page = 0, l = 25) {
  return ApiCall.wsCall('/list/0/', {
    p: page,
    l: l,
  });
}

export function getInfoAndChapterList(mangaId) {
  return ApiCall.wsCall(`manga/${mangaId}`);
}
export function getChapterImages(chapterId) {
  return ApiCall.wsCall(`chapter/${chapterId}`);
}

// export function confirmMobileCode(mobile, code) {
//   return ApiCall.wsCall('/User/ConfirmSubmit', {
//     mobile: mobile,
//     code: code
//   });
// }

// export function login(mobile, password, fcmToken) {
//   return ApiCall.wsCall('/User/Login', {
//     mobile: mobile,
//     password: password,
//     notificationId: fcmToken
//   });
// }

// export function logout() {
//   return ApiCall.wsCall('/User/Logout');
// }

// export function getCodeToChangePassword(mobile) {
//   return ApiCall.wsCall('/User/GetCodeToChangePassword', {
//     mobile: mobile
//   });
// }

// export function confirmChangePassword(mobile, code) {
//   return ApiCall.wsCall('/User/ConfirmChangePassword', {
//     mobile: mobile,
//     code: code
//   });
// }

// export function changePassword(mobile, oldPassword, newPassword) {
//   return ApiCall.wsCall('/User/ChangePassword', {
//     mobile: mobile,
//     confirmCode: oldPassword,
//     Password: newPassword
//   });
// }

// export function setNotificationId(fcmToken) {
//   return ApiCall.wsCall('/Notify/SetNotificationId', {
//     data: fcmToken
//   });
// }

// export function getAppLastVersion() {
//   let deviceInfo = {
//     OS_Version: Platform.Version,
//     SystemVersion: DeviceInfo.getSystemVersion(),
//     Brand: DeviceInfo.getBrand(),
//     Manufacturer: DeviceInfo.getManufacturer(),
//     Model: DeviceInfo.getModel(),
//     OS_AppVersion: DeviceInfo.getVersion(),
//     RN_AppVersion: appVersion
//   };

//   return ApiCall.wsCall('/Utils/GetAppLastVersion', {
//     data: {
//       version: appVersion,
//       deviceInfo: JSON.stringify(deviceInfo)
//     }
//   });
// }

// export function editProfile(firstName, lastName, image, email, onUploadProgress = null) {
//   return ApiCall.wsCall(
//     '/User/EditProfile',
//     {
//       data: {
//         firstName: firstName,
//         lastName: lastName,
//         image: image,
//         email: email
//       }
//     },
//     onUploadProgress
//   );
// }

// export function getChatList() {
//   return ApiCall.wsCall('/Notify/GetMyMessageGroups');
// }

// export function getChatMessages(chatListId, lastId, afterMessageId, pageSize) {
//   return ApiCall.wsCall('/Notify/ReadChat', {
//     data: {
//       chatListId: chatListId,
//       fromMessageId: lastId,
//       afterMessageId: afterMessageId
//     },
//     pageSize: pageSize
//   });
// }

// export function sendMessage(myId, userId, chatListId, message) {
//   return ApiCall.wsCall('/Notify/SendMessage_ForApp', {
//     data: {
//       From: myId,
//       To: userId,
//       Text: message,
//       chatListId: chatListId
//     }
//   });
// }

// export function getMyOTPList() {
//   return ApiCall.wsCall('/OTP/GetUserOTPList');
// }

// export function getOTPList() {
//   return ApiCall.wsCall('/OTP/GetOTPList');
// }

// export function requestOTP(systemId) {
//   return ApiCall.wsCall('/OTP/RequestOTP', {
//     data: systemId
//   });
// }

// export function removeUserOTP(systemId) {
//   return ApiCall.wsCall('/OTP/RemoveUserOTP', {
//     data: systemId
//   });
// }
