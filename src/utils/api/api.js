import axios from 'axios';
import aeCodes from './apiErrorCodes';
import { decoding } from '../Utils';
import { Alert } from 'react-native';

export default class Newapi {
  static instance = null;
  static getInstance() {
    if (this.instance == null) {
      this.instance = new Newapi();
    }

    return this.instance;
  }

  constructor() {
    axios.interceptors.response.use(
      function(response) {
        DebugLog('========= RESPONSE =========');
        DebugLog(JSON.stringify(response));
        DebugLog('============================');

        if (response == null || response.data == null) return Promise.reject(aeCodes.WS_ERROR);

        let responseJson = response.data;
        // Alert.alert('responseJson', JSON.stringify(responseJson));
        return responseJson;

        // if (responseJson.ResultCode != 0 || responseJson.ObjList == null)
        //   return Promise.reject({
        //     code: responseJson.ResultCode,
        //     message: responseJson.ResultMessage,
        //   })
        // else if (responseJson.ObjList != null && responseJson.ResultCode == 0) return responseJson
        // else return Promise.reject(aeCodes.WS_ERROR)
      },
      function(error) {
        DebugLog('========= ERROR =========');
        DebugLog(JSON.stringify(error));
        DebugLog('============================');
        console.log();

        if (axios.isCancel(error)) {
          return Promise.reject(aeCodes.WS_CANCEL);
        } else return Promise.reject(aeCodes.WS_CONNECT_ERROR);
      },
    );
  }
  // data = {},
  wsCall(url, params = {}, baseURL = 'https://www.mangaeden.com/api', onUploadProgress = null) {
    const CancelToken = axios.CancelToken;
    let cancel;

    var config = {
      method: 'get',
      baseURL: baseURL,
      url: url,
      headers: {
        'Content-Type': 'application/json',
        site: 'https://www.mangaeden.com/',
      },

      timeout: 20000,
      // data: {
      //   ...data,
      // },

      params: {
        ...params,
      },

      cancelToken: new CancelToken(function executor(c) {
        cancel = c;
      }),
      onUploadProgress: progressEvent => {
        if (onUploadProgress) {
          onUploadProgress(Math.round((progressEvent.loaded * 100) / progressEvent.total));
        }
      },
    };

    return {
      call: async () => {
        DebugLog(`+++++++++ ${baseURL}${url}+++++++++`);
        DebugLog('params:' + JSON.stringify(params));
        DebugLog('++++++++++++++++++++++++++');

        return axios(config);
      },
      cancel,
    };
  }
}
