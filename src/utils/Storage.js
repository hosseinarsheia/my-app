import { AsyncStorage } from 'react-native';

const storageName = 'NTSW';

export const KEYS = {
  USER_INFO: 'userInfo',
  MANGAS_INFO: 'mangasInfo',
};

export async function saveItem(key, value) {
  try {
    await AsyncStorage.setItem(`@${storageName}:${key}`, value.toString());
    return true;
  } catch (err) {
    return false;
  }
}

export async function getItem(key, def = '') {
  try {
    const value = await AsyncStorage.getItem(`@${storageName}:${key}`);
    return value !== null ? value : def;
  } catch (error) {
    return def;
  }
}

export async function removeItem(key) {
  try {
    await AsyncStorage.removeItem(`@${storageName}:${key}`);
    return true;
  } catch (err) {
    return false;
  }
}
