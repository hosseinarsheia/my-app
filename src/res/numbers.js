const numbers = {
  wsCallRetryTime: 3000,
  pageSize: 20,
  borderRadius: 5,
  elevation: 3,
}

export default numbers
