const fonts = {
  IRANSansMobile: 'IRANSansMobile',
  IRANSansMobile_Bold: 'IRANSansMobile_Bold',
  IRANSansMobile_Light: 'IRANSansMobile_Light',
}
export default fonts
