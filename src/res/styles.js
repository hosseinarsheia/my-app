import { StyleSheet } from 'react-native';
import strings from './strings';
import images from './images';
import colors from './colors';
import numbers from './numbers';
import dimensions from './dimensions';
import fontSizes from './fontSizes';
import fonts from './fonts';

const R = {
  strings,
  images,
  colors,
  dimensions,
  fontSizes,
  fonts,
  numbers,
};

const styles = StyleSheet.create({
  backgroundImage: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '200%',
    height: '200%',
  },
  headerFont: {
    fontSize: R.fontSizes.xBig,
    fontFamily: R.fonts.IRANSansMobile,
  },
  normalFont: {
    fontSize: R.fontSizes.fs15,
    color: R.colors.textColor,
    fontWeight: 'normal',
  },
  boldFont: {
    fontSize: R.fontSizes.normal,
    fontFamily: R.fonts.IRANSansMobile_Bold,
    color: R.colors.textColor,
  },
  smallFont: {
    fontSize: R.fontSizes.fs13,
    fontWeight: 'normal',
  },
  xSmallFont: {
    fontSize: R.fontSizes.fs11,
    fontFamily: R.fonts.IRANSansMobile,
    color: R.colors.textColor,
    textAlign: 'center',
  },
  smallFontBold: {
    fontSize: R.fontSizes.fs11,
    fontFamily: R.fonts.IRANSansMobile_Bold,
    color: R.colors.textColor,
  },

  border: {
    borderWidth: 1,
    borderColor: R.colors.borderColor,
  },
  borderRadius: {
    borderWidth: 1,
    borderRadius: R.numbers.borderRadius,
    borderColor: R.colors.borderColor,
  },
  errorText: {
    fontFamily: R.fonts.IRANSansMobile,
    fontSize: R.fontSizes.xxxBig,
    color: R.colors.error,
    textAlign: 'center',
    marginTop: 0,
  },
});

export default styles;
