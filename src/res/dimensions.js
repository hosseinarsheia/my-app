import { scale, verticalScale, moderateScale } from 'react-native-size-matters';
import { Dimensions } from 'react-native';

const window = Dimensions.get('window');
const screen = Dimensions.get('screen');

const dimensions = {
  /******************* Components Sizes  ******************/
  window: window,
  screen: screen,
  actionBarSize: 55,
  wizarheight: 40,
  wizardBorderRadius: 25,
  buttonHeight: 45,
  buttonIcon: verticalScale(25),
  pickerWidth: '95%',
  myInputHeight: verticalScale(45),
  errorCodeBoxPadding: verticalScale(12),
  dHeaderSize: 150,
  dAvatarSize: 100,

  /*******************  ******************/

  eIconSize: moderateScale(22),

  /******************* Main Sizes  ******************/
  h1: scale(1),
  h2: scale(2),
  h3: scale(3),
  h5: scale(5),
  h10: scale(10),
  h15: scale(15),
  h20: scale(20),
  h25: scale(25),
  h30: scale(30),
  h35: scale(35),
  h40: scale(40),
  h45: scale(45),
  h50: scale(50),
  h55: scale(55),
  h60: scale(60),
  h65: scale(65),
  h70: scale(70),
  h75: scale(75),
  h80: scale(80),

  v1: verticalScale(1),
  v2: verticalScale(2),
  v3: verticalScale(3),
  v5: verticalScale(5),
  v10: verticalScale(10),
  v15: verticalScale(15),
  v20: verticalScale(20),
  v25: verticalScale(25),
  v30: verticalScale(30),
  v35: verticalScale(35),
  v40: verticalScale(40),
  v45: verticalScale(45),
  v50: verticalScale(50),
  v55: verticalScale(55),
  v60: verticalScale(60),
  v65: verticalScale(65),
  v70: verticalScale(70),
  v200: verticalScale(200),

  b1: moderateScale(1),
  b2: moderateScale(2),
  b3: moderateScale(3),
  b5: moderateScale(5),
  b10: moderateScale(10),
  b15: moderateScale(15),
  b20: moderateScale(20),
  b25: moderateScale(25),
  b30: moderateScale(30),
  b35: moderateScale(35),
  b40: moderateScale(40),
  b45: moderateScale(45),
  b50: moderateScale(50),
  b55: moderateScale(55),
  b60: moderateScale(60),
  b65: moderateScale(65),
  b70: moderateScale(70),
};

export default dimensions;

// loadingSize: moderateScale(50),
// addIncomingEntryIconSize: moderateScale(60),
// addStuffPlusIcond: verticalScale(60),
// pbHeight: verticalScale(20),
// flatListLoadingIcon: moderateScale(50),
// diagramSize: moderateScale(100),
// diagramRectSize: moderateScale(12),
// barCodeWidth: scale(250),
// barCodeHeight: verticalScale(150),
// flashLightSize: moderateScale(55),
// barcodeIconSize: moderateScale(50),
// myPadding: verticalScale(30),

// myInputBorder: 1,
// borderRadius: 5,

// bottomMenuIconSizes: moderateScale(25),
// diagramRefreshIndicator: verticalScale(350),

// opendMessag
