import React from 'react'
import { View } from 'react-native'
import R from '../../res/R'
// import styles from '../MyInput/styles';
import Spinner from 'react-native-spinkit'

const Loading = props => {
  return (
    <View style={[{ width: '100%', alignItems: 'center', justifyContent: 'center' }, props.containerStyle]}>
      <Spinner
        isVisible={props.visible}
        color={props.loadingColor || R.colors.primaryColor}
        type={props.loadingType || 'WanderingCubes'}
        size={props.spinSize || R.dimensions.loadingSize}
      />
    </View>
  )
}
export default Loading
