import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import R from '../../res/R';

class RowWrapper extends Component {
  render() {
    let { children, title, textStyle, containerStyle, VerticalAlign, ...otherProps } = this.props;
    return (
      <View style={[styles.container, containerStyle]} {...otherProps}>
        {children}
        <Text style={[styles.text, { textAlignVertical: VerticalAlign ? 'center' : 'bottom', textStyle }, textStyle]}>
          {title}
        </Text>
      </View>
    );
  }
}

export default RowWrapper;

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%'
  },
  text: {
    ...R.styles.normalFont,
    flex: 0.4,
    textAlign: 'right',
    margin: 0
  }
});

RowWrapper.propTypes = {
  title: PropTypes.string,
  textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
};

RowWrapper.defaultProps = {
  VerticalAlign: false
};
