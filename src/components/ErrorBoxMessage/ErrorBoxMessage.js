import React, { Component } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { observable } from 'mobx'
import { inject, observer } from 'mobx-react'
import { Icon } from 'react-native-elements'

import R from '../../res/R'
import { styles } from './style'

@observer
class ErrorBoxMessage extends Component {
  @observable showBox = false
  @observable boxText = this.props.boxText
  @observable backgroundColor = this.props.backgroundColor
  @observable timer = null

  //close box after some time
  showErrorBoxMessage = (text, color = R.colors.errorBoxGreen, time = 3000) => {
    this.showBox = true
    if (text) this.boxText = text
    if (color) this.backgroundColor = color

    this.timer = setTimeout(() => {
      this.showBox = false
    }, time)
  }

  closeBoxHandler = () => {
    clearTimeout(this.timer)
    this.showBox = false
  }

  render() {
    const { containerStyle } = this.props

    return this.showBox ? (
      <TouchableOpacity
        activeOpacity={1}
        style={[styles.container, { backgroundColor: this.backgroundColor, ...containerStyle }]}
        onPress={this.closeBoxHandler}>
        <Icon
          size={R.dimensions.b25}
          name={this.backgroundColor == R.colors.errorBoxGreen ? 'check-circle' : 'times-circle'}
          type="font-awesome"
          color="white"
          onPress={this.closeBoxHandler}
          containerStyle={styles.iconContainerStyle}
        />
        <Text style={styles.textStyle}>{this.boxText}</Text>
      </TouchableOpacity>
    ) : null
  }
}

export default ErrorBoxMessage

ErrorBoxMessage.propTypes = {
  backgroundColor: PropTypes.string,
  boxText: PropTypes.string,
  showBox: PropTypes.bool,
  containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
}

ErrorBoxMessage.defaultProps = {
  backgroundColor: R.colors.green,
  boxText: '',
  showBox: false,
}
