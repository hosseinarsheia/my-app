import { StyleSheet } from 'react-native'

import R from '../../res/R'

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row-reverse',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    flex: 1,
    zIndex: 1000,
    paddingVertical: R.dimensions.errorCodeBoxPadding,
    paddingHorizontal: R.dimensions.h5,
  },
  iconContainerStyle: {
    padding: R.dimensions.b5,
    marginLeft: R.dimensions.h5,
  },
  textStyle: {
    ...R.styles.smallFont,
    color: 'white',
    flex: 1,
    textAlign: 'right',
  },
})
