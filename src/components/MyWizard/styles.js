import R from '../../res/R'
import { StyleSheet } from 'react-native'
import { moderateScale, scale, verticalScale } from 'react-native-size-matters'

export const MyWizardStyles = StyleSheet.create({
  wizardWrapper: {
    height: R.dimensions.wizarheight,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    margin: R.dimensions.h10,
    width: '95%',
    alignItems: 'stretch',
    alignSelf: 'center', // for centering in parent component
  },
  firstPartWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderBottomLeftRadius: R.dimensions.wizardBorderRadius,
    borderTopLeftRadius: R.dimensions.wizardBorderRadius,
    backgroundColor: 'white',
    borderColor: R.colors.borderColor,
    borderRightWidth: 0,
  },
  secondPartWrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderRightWidth: 0,
    borderColor: R.colors.borderColor,
    backgroundColor: 'white',
  },
  thirdPartWrapper: {
    flex: 1.1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: R.colors.borderColor,
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
  },
  fourthPartWrapper: {
    flex: 1.1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderBottomRightRadius: R.dimensions.wizardBorderRadius,
    borderTopRightRadius: R.dimensions.wizardBorderRadius,
    backgroundColor: 'white',
    borderColor: R.colors.borderColor,
  },
})
