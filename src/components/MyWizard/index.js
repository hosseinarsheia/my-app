import React, { Component } from 'react';
import { View, Text } from 'react-native';
import R from '../../res/R';
import { MyWizardStyles as styles } from './styles';
import PropTypes from 'prop-types';

class MyWizard extends Component {
  render() {
    const {
      firstText,
      firstTextWrapper,
      firstTextStyle,

      secondSection,
      secondText,
      secondTextWrapper,
      secondTextStyle,

      thirdSection,
      thirdText,
      thirdTextWrapper,
      thirdTextStyle,

      fourthText,
      fourthTextWrapper,
      fourthTextStyle
    } = this.props;
    return (
      <View style={styles.wizardWrapper}>
        <View style={[styles.firstPartWrapper, { ...firstTextWrapper }]}>
          <Text
            style={[thirdSection && secondSection ? R.styles.smallFont : R.styles.normalFont, { ...firstTextStyle }]}>
            {firstText}
          </Text>
        </View>

        {secondSection ? (
          <View style={[styles.secondPartWrapper, { ...secondTextWrapper }]}>
            <Text
              style={[
                thirdSection && secondSection ? R.styles.smallFont : R.styles.normalFont,
                { ...secondTextStyle }
              ]}>
              {secondText}
            </Text>
          </View>
        ) : null}

        {thirdSection ? (
          <View style={[styles.thirdPartWrapper, { ...thirdTextWrapper }]}>
            <Text
              style={[thirdSection && secondSection ? R.styles.smallFont : R.styles.normalFont, { ...thirdTextStyle }]}>
              {thirdText}
            </Text>
          </View>
        ) : null}

        <View style={[styles.fourthPartWrapper, { ...fourthTextWrapper }]}>
          <Text
            style={[thirdSection && secondSection ? R.styles.smallFont : R.styles.normalFont, { ...fourthTextStyle }]}>
            {fourthText}
          </Text>
        </View>
      </View>
    );
  }
}

export default MyWizard;

MyWizard.propTypes = {
  firstText: PropTypes.string,
  secondText: PropTypes.string,
  thirdText: PropTypes.string,
  fourthText: PropTypes.string,

  firstTextWrapper: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  secondTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  thirdTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  fourthTextWrapper: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),

  firstTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  fourthTextWrapper: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  fourthTextWrapper: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  fourthTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
};

MyWizard.defaultProps = {
  secondSection: false,
  thirdSection: false
};
