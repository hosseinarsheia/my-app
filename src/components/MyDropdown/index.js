import React, { Component } from 'react';
import R from '../../res/R';
import { Dropdown } from 'react-native-material-dropdown';
import PropTypes from 'prop-types';

class MyDropdown extends Component {
  render() {
    const {
      data,
      itemTextStyle,
      fontSize,
      animationDuration,
      style,
      dropdownOffset,
      rippleInsets,
      onChangeText,
      pickerStyle,
      itemCount,

      ...otherProps
    } = this.props;

    return (
      <Dropdown
        data={data}
        itemTextStyle={itemTextStyle}
        fontSize={fontSize}
        animationDuration={animationDuration}
        style={[style, { height: R.fontSizes.normal * 1.8, textAlign: 'center' }]}
        dropdownOffset={dropdownOffset}
        rippleInsets={rippleInsets}
        onChangeText={onChangeText}
        pickerStyle={pickerStyle}
        itemCount={itemCount}
        {...otherProps}
      />
    );
  }
}

export default MyDropdown;

MyDropdown.propTypes = {
  fontSize: PropTypes.number,
  animationDuration: PropTypes.number,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  dropdownOffset: PropTypes.object,
  rippleInsets: PropTypes.object,
  onChangeText: PropTypes.func,
  pickerStyle: PropTypes.object,
  itemCount: PropTypes.number,
  itemTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  titleTextStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]) // error masseage style
};

MyDropdown.defaultProps = {
  fontSize: R.fontSizes.normal,
  animationDuration: R.numbers.animationDuration,
  style: R.styles.normalFont,
  itemTextStyle: [R.styles.normalFont, { textAlign: 'center' }],
  dropdownOffset: { top: 9, left: 0 },
  rippleInsets: { top: 0, bottom: -8 },
  pickerStyle: { width: R.dimensions.pickerWidth },
  itemCount: 8,
  titleTextStyle: {
    fontFamily: R.fonts.IRANSansMobile,
    fontSize: R.fontSizes.small,
    color: R.colors.error,
    textAlign: 'center',
    marginTop: 0
  }
};
