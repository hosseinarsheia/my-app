import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { CheckBox } from 'react-native-elements'
import PropTypes from 'prop-types'

import R from '../../res/R'

class MyCheckBox extends Component {
  render() {
    let { title, containerStyle, checked, checkedColor, textStyle } = this.props
    return (
      <CheckBox
        right={true}
        iconRight={true}
        title={title}
        containerStyle={[styles.containerStyle, { ...containerStyle }]}
        textStyle={[styles.textStyle, { ...textStyle }]}
        checked={checked}
        checkedColor={checkedColor}
        {...this.props}
      />
    )
  }
}

export const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    padding: 0,
    margin: 0,
    width: '100%',
    paddingBottom: R.dimensions.v10,
    paddingRight: R.dimensions.v15,
  },
  textStyle: {
    ...R.styles.normalFont,
    textAlign: 'right',
    fontWeight: 'normal',
  },
})

MyCheckBox.propTypes = {
  containerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
  textStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array]),
  title: PropTypes.string,
  checked: PropTypes.bool,
  checkedColor: PropTypes.string,
}

MyCheckBox.defaultProps = {
  checkedColor: R.colors.primaryColor,
}

export default MyCheckBox
