import React, { Component } from 'react'
import { View, TouchableWithoutFeedback, Modal } from 'react-native'
import { observer } from 'mobx-react'
import R from '../../res/R'

@observer
export default class MyModal extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Modal
        style={{ width: '100%', height: '100%' }}
        visible={this.props.visible}
        transparent={true}
        onRequestClose={() => {
          if (this.props.onRequestClose && typeof this.props.onRequestClose === 'function') this.props.onRequestClose()
        }}>
        <TouchableWithoutFeedback
          onPress={() => {
            if (this.props.onTouchOutside && typeof this.props.onTouchOutside === 'function')
              this.props.onTouchOutside()
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: R.colors.modalBack,
            }}>
            <TouchableWithoutFeedback onPress={() => {}}>
              <View
                style={[
                  {
                    ...R.styles.border,
                    backgroundColor: R.colors.white,
                    paddingVertical: R.dimensions.v10,
                    paddingHorizontal: R.dimensions.h10,
                  },
                  this.props.panelStyle,
                ]}>
                {this.props.children}
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    )
  }
}
