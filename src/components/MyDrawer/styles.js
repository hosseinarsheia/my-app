import R from '../../res/R'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  header: {
    height: R.dimensions.dHeaderSize,
    backgroundColor: R.colors.primaryColor,
  },
  avatar: {
    height: R.dimensions.dAvatarSize,
    aspectRatio: 1,
    alignSelf: 'center',
    marginTop: -(R.dimensions.dAvatarSize / 2),
    backgroundColor: 'white',
    borderRadius: 50,
  },
  userInfoText: {
    fontFamily: R.fonts.IRANSansMobile,
    fontSize: R.fontSizes.small,
    color: R.colors.text_gray_dark,
    alignSelf: 'center',
    textAlign: 'center',
    marginVertical: R.dimensions.v35,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingHorizontal: R.dimensions.h35,
    paddingVertical: R.dimensions.v5,
  },
  itemText: {
    fontFamily: R.fonts.IRANSansMobile,
    fontSize: R.fontSizes.normal,
    color: R.colors.black,
    alignSelf: 'flex-end',
    textAlign: 'right',
    color: R.colors.textColor,
  },
  appVersionText: {
    fontFamily: R.fonts.IRANSansMobile,
    fontSize: R.fontSizes.fs11,
    color: R.colors.text_label,
    // alignSelf: 'center',
    textAlign: 'center',
    marginVertical: R.dimensions.v35,
  },
  radioButtonIcon: {
    width: R.dimensions.b10,
    height: R.dimensions.b10,
    backgroundColor: R.colors.primaryColor,
    borderRadius: 20,
    marginLeft: R.dimensions.h5,
  },
  textSectionWrapper: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  iconContainerStyle: {
    minWidth: '15%',
    flexDirection: 'row',
    justifyContent: 'center',
    marginLeft: R.dimensions.h5,
  },
})
