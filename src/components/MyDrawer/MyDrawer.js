import React, { Component } from 'react'
import { View, ScrollView, TouchableHighlight, Text, Linking, Image, ActivityIndicator, Alert } from 'react-native'
import { SafeAreaView } from 'react-navigation'
import { Icon } from 'react-native-elements'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { default as NativeToast } from 'react-native-simple-toast'

import Navigation from '../../utils/Navigation'
import styles from './styles'
import R from '../../res/R'
import { userLogOut, getUserInfo, ROLE } from '../../utils/Utils'
import MyAlert from '../MyAlert'
import UpdateModal from '../../screens/SplashScreen/UpdateModal'
import Spinner from 'react-native-spinkit'
import * as Api from '../../utils/api'
import * as Storage from '../../utils/Storage'
import { scrollRef, guideStart } from '../../screens/MainScreen'
import MyModal from '../MyModal'

@observer
export default class MyDrawer extends Component {
  @observable versionChecking = false
  @observable userHaveRole = null
  @observable modalVisibilty = false

  constructor(props) {
    super(props)
  }

  navigate = screen => {
    this.props.navigation.closeDrawer()
    setTimeout(() => {
      this.props.navigation.navigate(screen)
    }, 100)
  }

  checkAppVersion = async () => {
    this.versionChecking = true

    const userInfo = await getUserInfo()

    Api.getAppLastVersion(userInfo.nationalCode)
      .call()
      .then(async lastVersionResponse => {
        // Alert.alert('lastVersionResponse', JSON.stringify(lastVersionResponse));
        const response = lastVersionResponse.ObjList[0]

        if (response.LastVersion > appVersion) this.updateModal.show(response.UrlPath, response.NeedForUpdate)
        else {
          NativeToast.show(R.strings.appVersionIsLatestVersion, NativeToast.LONG)
        }

        this.props.navigation.closeDrawer()
      })
      .catch(err => {
        if (err.message) this.myAlert.showAlert(err.message)
        else this.myAlert.showAlert(R.strings.anErrorOccurred)
      })
      .finally(() => {
        this.versionChecking = false
      })
  }

  logout = () => {
    this.myAlert.showAlert(R.strings.exitAppQuestion, [
      {
        text: R.strings.yes,
        onPress: async () => {
          try {
            this.modalVisibilty = true
            await userLogOut()
            this.props.navigation.closeDrawer()
          } catch (error) {}
          this.modalVisibilty = false
        },
      },
      {
        text: R.strings.cancel,
      },
    ])
  }

  componentDidMount = () => {
    this.callInDidMount()
  }

  // change App Structure when user Have No Role
  callInDidMount = async () => {
    try {
      const userInfo = JSON.parse(await Storage.getItem(Storage.KEYS.HAVE_ROLE, '{}'))
      this.userHaveRole = userInfo.userRoleStatus
    } catch (error) {}
  }

  // <Image style={styles.avatar} source={R.images.splash_logo} resizeMode='cover' />

  render() {
    return (
      <ScrollView>
        <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always', horizontal: 'never' }}>
          <MyAlert ref={r => (this.myAlert = r)} />

          <UpdateModal ref={r => (this.updateModal = r)} />

          <MyModal visible={this.modalVisibilty}>
            <ActivityIndicator animating={true} color={R.colors.primaryColor} size={R.dimensions.flatListLoadingIcon} />
          </MyModal>

          <View style={styles.header} />

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: -((R.dimensions.dAvatarSize + R.dimensions.b10 * 2) / 2),
              backgroundColor: 'white',
              borderRadius: 100,
              alignSelf: 'center',
              borderWidth: 2,
              borderColor: R.colors.primaryDarkColor,
              padding: R.dimensions.b10,
            }}>
            <Image
              style={{ width: R.dimensions.dAvatarSize, height: R.dimensions.dAvatarSize }}
              source={R.images.splash_logo}
              resizeMode="contain"
            />
          </View>

          {/* 
            <TouchableHighlight
            style={[styles.itemContainer, { marginTop: R.dimensions.v25 }]}
            underlayColor={R.colors.buttonGrayUnderlay}
            onPress={() => this.navigate(Navigation.SCREENS.DOC_INQUIRY_FIRST_SCREEN)}>
            <View style={styles.textSectionWrapper}>
              <Text style={styles.itemText}>{R.strings.docsManagement}</Text>
              <View style={styles.radioButtonIcon} />
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.itemContainer}
            underlayColor={R.colors.buttonGrayUnderlay}
            onPress={() => this.navigate(Navigation.SCREENS.VIEW_INVENTORY)}>
            <View style={styles.textSectionWrapper}>
              <Text style={styles.itemText}>{R.strings.viewStuffInventory}</Text>
              <View style={styles.radioButtonIcon} />
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.itemContainer}
            underlayColor={R.colors.buttonGrayUnderlay}
            onPress={() => this.navigate(Navigation.SCREENS.Product_Inquiry_First_Screen)}>
            <View style={styles.textSectionWrapper}>
              <Text style={styles.itemText}>{R.strings.incomingStuffEntry}</Text>
              <View style={styles.radioButtonIcon} />
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.itemContainer}
            underlayColor={R.colors.buttonGrayUnderlay}
            onPress={() => this.navigate(Navigation.SCREENS.TransferOwnershipAndPlaceFirstScreen)}>
            <View style={styles.textSectionWrapper}>
              <Text style={styles.itemText}>{R.strings.transferOwnershipAndPlace}</Text>
              <View style={styles.radioButtonIcon} />
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.itemContainer}
            underlayColor={R.colors.buttonGrayUnderlay}
            onPress={() => this.navigate(Navigation.SCREENS.Product_TakeOut_First_Screen)}>
            <View style={styles.textSectionWrapper}>
              <Text style={styles.itemText}>{R.strings.outgoingStuffEntry}</Text>
              <View style={styles.radioButtonIcon} />
            </View>
          </TouchableHighlight>
         */}

          <TouchableHighlight
            style={[styles.itemContainer, { marginTop: R.dimensions.v35 }]}
            underlayColor={R.colors.buttonGrayUnderlay}
            onPress={() => {
              this.props.navigation.closeDrawer()

              scrollRef.scrollTo({ x: 0, y: 0, animated: true })
              setTimeout(() => {
                guideStart()
              }, 10)
            }}>
            <View style={styles.textSectionWrapper}>
              <Text style={styles.itemText}>{R.strings.appGuide}</Text>
              <Icon
                name="question-circle"
                type="font-awesome"
                color={R.colors.primaryColor}
                size={R.dimensions.b25}
                containerStyle={styles.iconContainerStyle}
              />
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.itemContainer}
            underlayColor={R.colors.buttonGrayUnderlay}
            onPress={() => Linking.openURL(`tel:02175492000`)}>
            <View style={styles.textSectionWrapper}>
              <Text style={styles.itemText}>{R.strings.callSupportUnit}</Text>
              <Icon
                name="call"
                type="material"
                color={R.colors.primaryColor}
                size={R.dimensions.b25}
                containerStyle={styles.iconContainerStyle}
              />
            </View>
          </TouchableHighlight>

          <TouchableHighlight
            style={styles.itemContainer}
            underlayColor={R.colors.buttonGrayUnderlay}
            onPress={() => this.checkAppVersion()}>
            <View style={styles.textSectionWrapper}>
              <Spinner
                isVisible={this.versionChecking}
                color={R.colors.primaryColor}
                type={'WanderingCubes'}
                size={R.dimensions.loadingSize / 2}
              />
              <Text style={[styles.itemText, { marginLeft: R.dimensions.h10 }]}>{R.strings.checkAppVersion}</Text>
              <Icon
                name="sync"
                type="material"
                color={R.colors.primaryColor}
                size={R.dimensions.b25}
                containerStyle={styles.iconContainerStyle}
              />
            </View>
          </TouchableHighlight>

          {this.userHaveRole == ROLE.HAVEROLE ? (
            <TouchableHighlight
              style={styles.itemContainer}
              underlayColor={R.colors.buttonGrayUnderlay}
              onPress={() => this.navigate(Navigation.SCREENS.CHOOSE_USER_ACCESS)}>
              <View style={styles.textSectionWrapper}>
                <Text style={styles.itemText}>{R.strings.changeRole}</Text>
                <Icon
                  name="person"
                  type="material"
                  color={R.colors.primaryColor}
                  size={R.dimensions.b25}
                  containerStyle={styles.iconContainerStyle}
                />
              </View>
            </TouchableHighlight>
          ) : null}

          <TouchableHighlight
            style={styles.itemContainer}
            underlayColor={R.colors.buttonGrayUnderlay}
            onPress={() => this.logout()}>
            <View style={styles.textSectionWrapper}>
              <Text style={styles.itemText}>{R.strings.logout}</Text>
              <Icon
                name="exit-to-app"
                type="material"
                color={R.colors.primaryColor}
                size={R.dimensions.b25}
                containerStyle={styles.iconContainerStyle}
              />
            </View>
          </TouchableHighlight>

          <Text style={styles.appVersionText}>{R.getString(R.strings.appVersion, appVersionName)}</Text>
        </SafeAreaView>
      </ScrollView>
    )
  }
}
