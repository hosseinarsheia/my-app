import { StyleSheet } from 'react-native'
import R from '../../res/R'

export const styles = StyleSheet.create({
  container: {
    minHeight: R.dimensions.myInputHeight,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 0,
    paddingRight: 0,
  },
  inputContainer: {
    width: '100%',
    borderRadius: R.dimensions.myInputHeight / 10,
    borderWidth: R.dimensions.myInputBorder,
    borderColor: R.colors.primaryDarkColor,
    backgroundColor: R.colors.myInputBack,
    // borderColor: 'transparent',
    justifyContent: 'center',
  },
  inputStyle: {
    flex: 1,
    fontFamily: R.fonts.IRANSansMobile,
    fontSize: R.fontSizes.normal,
    color: R.colors.black,
    paddingRight: R.dimensions.h5,
    textAlign: 'center',
    textAlignVertical: 'bottom',
    padding: 0,
  },
  label: {
    fontFamily: R.fonts.IRANSansMobile,
    fontSize: R.fontSizes.normal,
    textAlign: 'center',
    marginBottom: R.dimensions.v5,
    fontWeight: '400',
  },
  error: {
    fontFamily: R.fonts.IRANSansMobile,
    fontSize: R.fontSizes.small,
    color: R.colors.error,
    textAlign: 'center',
    margin: 0,
  },
})
