import React, { Component, Fragment } from 'react';
import { View, Text, Alert, ScrollView, Image, StatusBar, ActivityIndicator } from 'react-native';
import FastImage from 'react-native-fast-image';
import { observable } from 'mobx';
import { inject, observer } from 'mobx-react';

import R from '../../res/R';
import Navigation from '../../utils/Navigation';
import * as Api from '../../utils/api';
import DoubleText from '../../components/DoubleText';
import { TouchableOpacity } from 'react-native-gesture-handler';

@observer
class MangaInformationScreen extends Component {
  @observable mangaInfo = Navigation.getParam(this.props, Navigation.PARAMS.MANGA_INFO, null);
  @observable statusBarheight = R.dimensions.v25;
  @observable chaptersInfo = {};

  componentDidMount = () => {
    this.statusBarheight = StatusBar.currentHeight;
    this.getMangaChapterList();
  };

  getMangaChapterList = () => {
    Api.getInfoAndChapterList(this.mangaInfo.i)
      .call()
      .then(res => {
        this.chaptersInfo = { ...res };
      })
      .catch(err => Alert.alert('err', JSON.stringify(err)));
  };

  render() {
    const { im, i } = this.mangaInfo;
    const { title, artist_kw, author_kw, categories, chapters_len, hits, description, chapters } = this.chaptersInfo;
    return (
      <View style={{ flex: 1, alignItems: 'center', paddingTop: this.statusBarheight }}>
        <StatusBar ref={ref => (this.mine = ref)} barStyle="dark-content" backgroundColor="rgba(0,0,0,0.4)" translucent />
        <Image
          source={{
            uri: `https://cdn.mangaeden.com/mangasimg/5c/5c3cfd7787e2d2a22f444a1f473204af42d68549bc7e48ea0a215244.png`,
          }}
          style={{ position: 'absolute', width: '100%', height: R.dimensions.window.height }}
          blurRadius={5}
        />

        <ScrollView style={{ width: '100%', backgroundColor: 'rgba(0,0,0,0.4)', paddingTop: R.dimensions.v5 }}>
          <Fragment>
            <View style={{ width: '100%', flexDirection: 'row' }}>
              <FastImage
                style={{ flex: 1, height: R.dimensions.window.height / 2 }}
                source={{
                  uri: `https://cdn.mangaeden.com/mangasimg/${im}`,
                  // uri: `https://cdn.mangaeden.com/mangasimg/2a/2ade708d91d55b2ee996fda1b11ec43484f47c9578a519bced31142a.jpg`,
                  priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.cover}
              />

              {this.chaptersInfo.artist ? (
                <View style={{ flex: 1, padding: R.dimensions.b10 }}>
                  <DoubleText
                    label="Title :"
                    secondText={title}
                    labelTextStyle={{ color: 'white', fontWeight: '700' }}
                    secondTextStyle={{ color: 'white' }}
                  />
                  <DoubleText
                    label="Author :"
                    secondText={author_kw.toString()}
                    labelTextStyle={{ color: 'white', fontWeight: '700' }}
                    secondTextStyle={{ color: 'white' }}
                  />
                  <DoubleText
                    label="Artist :"
                    secondText={artist_kw.toString()}
                    labelTextStyle={{ color: 'white', fontWeight: '700' }}
                    secondTextStyle={{ color: 'white' }}
                  />
                  <DoubleText
                    label="Categories :"
                    secondText={categories.toString()}
                    labelTextStyle={{ color: 'white', fontWeight: '700' }}
                    secondTextStyle={{ color: 'white' }}
                  />
                  <DoubleText
                    label="Chapters length :"
                    secondText={chapters_len}
                    labelTextStyle={{ color: 'white', fontWeight: '700' }}
                    secondTextStyle={{ color: 'white' }}
                  />
                  <DoubleText
                    label="Likes :"
                    secondText={hits}
                    labelTextStyle={{ color: 'white', fontWeight: '700' }}
                    secondTextStyle={{ color: 'white' }}
                  />
                  <DoubleText
                    label="Released Date :"
                    secondText={hits}
                    labelTextStyle={{ color: 'white', fontWeight: '700' }}
                    secondTextStyle={{ color: 'white' }}
                  />
                </View>
              ) : (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <ActivityIndicator size="large" />
                </View>
              )}
            </View>
            <View
              style={{
                flex: 1,
                width: '100%',
                padding: R.dimensions.b10,
                alignItems: 'center',
              }}>
              {this.chaptersInfo.artist ? (
                <Fragment>
                  <DoubleText
                    label="Description :"
                    labelTextStyle={{ color: 'white', fontWeight: '700' }}
                    secondText={description}
                    secondTextStyle={{ color: 'white' }}
                  />

                  {chapters.map(value => {
                    return (
                      <TouchableOpacity
                        style={{
                          borderRadius: R.numbers.borderRadius,
                          backgroundColor: '#00BFA5',
                          marginBottom: R.dimensions.v10,
                          padding: R.dimensions.b10,
                          paddingHorizontal: R.dimensions.h50,
                        }}
                        onPress={() => {
                          Navigation.navigate(Navigation.SCREENS.CHAPTER_IMAGES_SCREEN, [Navigation.PARAMS.CHAPTER_ID, value[3]]);
                        }}>
                        <Text
                          style={[
                            R.styles.normalFont,
                            {
                              width: R.dimensions.h80,
                              textAlign: 'center',
                              color: 'white',
                              fontWeight: '700',
                            },
                          ]}>
                          {value[0]}
                        </Text>
                      </TouchableOpacity>
                    );
                  })}
                </Fragment>
              ) : (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <ActivityIndicator size="large" />
                </View>
              )}
            </View>
          </Fragment>
        </ScrollView>
      </View>
    );
  }
}

export default MangaInformationScreen;
