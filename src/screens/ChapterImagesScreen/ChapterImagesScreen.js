import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, Image, ActivityIndicator } from 'react-native';
import { observable } from 'mobx';
import { inject, observer } from 'mobx-react';
import FastImage from 'react-native-fast-image';
import Swiper from 'react-native-swiper';

import R from '../../res/R';
import Navigation from '../../utils/Navigation';
import * as Api from '../../utils/api';

@observer
class ChapterImagesScreen extends Component {
  @observable chapterId = Navigation.getParam(this.props, Navigation.PARAMS.CHAPTER_ID, null);
  @observable chapterImages = [];

  componentDidMount = () => {
    this.getImages();
  };

  getImages = () => {
    Api.getChapterImages(this.chapterId)
      .call()
      .then(res => (this.chapterImages = res.images))
      .catch(err => Alert.alert('err', JSON.stringify(err)));
  };

  renderImages = () => {
    return (
      <Swiper>
        {this.chapterImages.map(value => {
          return (
            <FastImage
              style={{ flex: 1, width: null, height: null }}
              source={{
                uri: `https://cdn.mangaeden.com/mangasimg/${value[1]}`,
                priority: FastImage.priority.normal,
              }}
              resizeMode={FastImage.resizeMode.contain}
            />
          );
        })}
      </Swiper>
    );
  };
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'red' }}>
        <Image
          style={{ position: 'absolute', width: '100%', height: R.dimensions.window.height }}
          blurRadius={5}
          source={{
            uri: `https://cdn.mangaeden.com/mangasimg/5c/5c3cfd7787e2d2a22f444a1f473204af42d68549bc7e48ea0a215244.png`,
          }}
        />
        {this.chapterImages.length > 0 ? (
          this.renderImages()
        ) : (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size="large" />
          </View>
        )}
      </View>
    );
  }
}

export default ChapterImagesScreen;
