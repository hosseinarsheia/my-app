import React, { useEffect } from 'react';
import { View, Text, Alert, StyleSheet } from 'react-native';
import { Image } from 'react-native-elements';
import { observer, useLocalStore } from 'mobx-react';
import FastImage from 'react-native-fast-image';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Navigation from '../../utils/Navigation';

import R from '../../res/R';
import DoubleText from '../../components/DoubleText';
import MyCard from '../../components/MyCard';
// import Myinput from '../../components/Myinput';

const ListItem = props => {
  const { im, t, c } = props.item;
  //   useEffect(() => {
  //     if (props.index == 0) Alert.alert('props.item', JSON.stringify(props.item));
  //   }, []);
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.container}
      onPress={() => {
        // Alert.alert('props.item', props.item);
        Navigation.navigate(Navigation.SCREENS.MANGA_INFORMATION_SCREEN, [Navigation.PARAMS.MANGA_INFO, props.item]);
      }}>
      <FastImage
        style={styles.imageStyle}
        resizeMode={FastImage.resizeMode.contain}
        source={{
          uri: `https://cdn.mangaeden.com/mangasimg/${im}`,
          priority: FastImage.priority.normal,
        }}
      />

      <View style={styles.textWrapper}>
        <Text style={styles.textStyle}>
          Title: <Text style={[R.styles.smallFont, { color: 'black' }]}>{t}</Text>
        </Text>

        <Text style={styles.textStyle}>
          Category: <Text style={[R.styles.smallFont, { color: 'black' }]}>{c.toString()}</Text>
        </Text>
      </View>
    </TouchableOpacity>
  );
};
// <Text style={styles.textStyle}>Category: {c.toString()} </Text>

export default observer(ListItem);

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    marginBottom: R.dimensions.h5,
    backgroundColor: 'white',
    padding: R.dimensions.h5,
    paddingRight: 20,
    borderRadius: 5,
  },
  imageStyle: {
    width: 100,
    height: 100,
    // backgroundColor: 'red',
    borderRadius: R.numbers.borderRadius,
  },
  textStyle: {
    ...R.styles.smallFont,
    textAlign: 'left',
    color: R.colors.textColor,
  },
  textWrapper: {
    marginLeft: R.dimensions.h5,
    flex: 1,
  },
});
