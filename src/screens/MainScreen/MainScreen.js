import React, { Component, Fragment, useEffect, useState, useRef } from 'react';
import { View, Text, FlatList, Alert, StyleSheet, StatusBar, ActivityIndicator, Image } from 'react-native';
import { observer, useLocalStore } from 'mobx-react';
import { observable } from 'mobx';
import Swiper from 'react-native-swiper';
import FastImage from 'react-native-fast-image';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import BottomSheet from 'reanimated-bottom-sheet';

import R from '../../res/R';
import * as Api from '../../utils/api';
import ListItem from './ListItem';
import MyInput from '../../components/MyInput';
import * as Storage from '../../utils/Storage';

let imageArray = [
  '2a/2ade708d91d55b2ee996fda1b11ec43484f47c9578a519bced31142a.jpg',
  '5c/5c3cfd7787e2d2a22f444a1f473204af42d68549bc7e48ea0a215244.png',
  'b0/b0ac7f12d2cb0fc07b9418d5544a3f97cbbc30e967396ae70f98d101.png',
];
@observer
class MainScreen extends Component {
  @observable dataList = [];
  @observable filteredData = [];
  @observable searchText = '';
  @observable statusBarheight = R.dimensions.v25;

  componentDidMount = () => {
    this.statusBarheight = StatusBar.currentHeight;
    this.getMangaList();
  };

  getMangaList = async () => {
    const mangaInfo = JSON.parse(await Storage.getItem(Storage.KEYS.MANGAS_INFO, null));
    if (mangaInfo) {
      this.dataList = mangaInfo;
      this.filteredData = mangaInfo.filter(value => value.im);
    } else {
      Api.getAllMangaList()
        .call()
        .then(async res => {
          this.dataList = res.manga;
          this.filteredData = res.manga.filter(value => value.im);
          await Storage.saveItem(Storage.KEYS.MANGAS_INFO, JSON.stringify(res.manga));
        })
        .catch(err => Alert.alert('err', JSON.stringify(err)));
    }
  };

  searchHandler = () => {
    let searchText = this.searchText.toLowerCase();
    this.filteredData = this.dataList.filter(value => {
      return value.t.toLowerCase().includes(searchText) || value.a.toLowerCase().includes(searchText);
    });
  };

  refreshListHandler = async () => {
    await Storage.removeItem(Storage.KEYS.MANGAS_INFO);
    this.dataList = [];
    this.filteredData = [];
    this.getMangaList();
  };

  renderList = ({ item, index }) => <ListItem item={item} index={index} />;
  keyExtractor = item => `${item.i}`;

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', paddingTop: this.statusBarheight + 5 }}>
        <Image
          source={{
            uri: `https://cdn.mangaeden.com/mangasimg/2a/2ade708d91d55b2ee996fda1b11ec43484f47c9578a519bced31142a.jpg`,
          }}
          style={{ position: 'absolute', width: '100%', height: R.dimensions.window.height }}
          blurRadius={5}
        />

        <StatusBar ref={ref => (this.mine = ref)} barStyle="dark-content" backgroundColor="rgba(0,0,0,0)" translucent={true} />

        <View style={{ flex: 1 }}>
          <Swiper>
            {imageArray.map(value => {
              return (
                <TouchableOpacity key={value} activeOpacity={1} onPress={() => alert(`pressed`)}>
                  <FastImage
                    style={{ width: '100%', height: R.dimensions.screen.height / 2 }}
                    source={{
                      uri: `https://cdn.mangaeden.com/mangasimg/${value}`,
                      priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                </TouchableOpacity>
              );
            })}
          </Swiper>
        </View>

        <View
          style={{
            width: '100%',
            backgroundColor: '#EFEFEF',
            flex: 1,
            paddingTop: R.dimensions.v20,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            paddingHorizontal: R.dimensions.v10,
          }}>
          {this.dataList.length > 0 ? (
            <Fragment>
              <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center', marginBottom: 20 }}>
                <MyInput
                  containerStyle={{ minHeight: 30, backgroundColor: 'white', flex: 1 }}
                  inputContainerStyle={{ borderWidth: 0, borderColor: R.colors.borderColor, flex: 1 }}
                  inputStyle={{ textAlignVertical: 'center', padding: 0, borderWidth: 0 }}
                  onChangeText={text => {
                    this.searchText = text;
                    if (!this.searchText) this.filteredData = this.dataList.filter(value => value.im);
                  }}
                  placeholder="Search ..."
                  keyboardType="default"
                  value={this.searchText}
                  onSubmitEditing={this.searchHandler}
                  leftIcon={<Icon name="ios-search" type="ionicon" color="#517fa4" onPress={this.searchHandler} />}
                />

                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginHorizontal: R.dimensions.h5,
                    paddingHorizontal: R.dimensions.h5,
                  }}>
                  <Icon name="md-sync" type="ionicon" color="#517fa4" onPress={this.refreshListHandler} />
                </View>
              </View>

              <FlatList
                showsVerticalScrollIndicator={false}
                data={this.filteredData}
                renderItem={this.renderList}
                keyExtractor={this.keyExtractor}
              />
            </Fragment>
          ) : (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <ActivityIndicator size="large" />
            </View>
          )}
        </View>
      </View>
    );
  }
}

export default MainScreen;

export const styles = StyleSheet.create({
  flatListWrapper: {
    width: '100%',
    flex: 0.5,
    height: R.dimensions.screen.height / 2,
    backgroundColor: 'rgba(50,50,50,0.1)',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingTop: R.dimensions.v30,
  },
});

// realmSavehandler = arr => {
//   const MangaSchema = {
//     name: 'Manga',
//     // primaryKey: 'make',
//     properties: {
//       image: 'string?',
//       title: { type: 'string' },
//       id: { type: 'string' },
//       alias: { type: 'string' },
//     },
//   };

//   Realm.open({
//     schema: [MangaSchema],
//   }).then(realm => {

//     let allMangas = realm.objects('Book');
//     realm.delete(allBooks);
//     for (let value of arr) {
//       realm.write(() => {
//         realm.create('Manga', { image: value.im, title: value.t, id: value.i, alias: value.a });
//       });
//     }
//   });
// };
