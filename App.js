/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { Alert, AsyncStorage, View, Text } from 'react-native';
import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import { observer, Provider } from 'mobx-react';
import { observable } from 'mobx';

import * as Storage from './src/utils/Storage';
import { rootStore } from './src/utils/Store';
import Navigation from './src/utils/Navigation';

import MainScreen from './src/screens/MainScreen';
import MangaInformationScreen from './src/screens/MangaInformationScreen';
import ChapterImagesScreen from './src/screens/ChapterImagesScreen';
import { enableScreens } from 'react-native-screens';

global.appVersion = 1;
global.appVersionName = '1.0.0';

console.disableYellowBox = true;

global.DebugLog = (text, addBeginSeparator = false, addEndSeparator = false) => {
  if (__DEV__) {
    if (addBeginSeparator) console.log(`-----------------------`);
    console.log(text);
    if (addEndSeparator) console.log(`-----------------------`);
  }
};

enableScreens();

// const DrawerNavigator = createDrawerNavigator(
//   {
//     Main: {
//       screen: MainScreen,
//       navigationOptions: {},
//     },
//   },
//   {
//     drawerPosition: 'right',
//     contentComponent: MyDrawer,
//     drawerType: 'front',
//     drawerLockMode: 'locked-closed', // prevent drawer to open by pan gesture
//   },
// );

const AppNavigator = createStackNavigator(
  {
    MainScreen: MainScreen,
    MangaInformationScreen: MangaInformationScreen,
    ChapterImagesScreen: ChapterImagesScreen,
  },
  {
    initialRouteName: 'MainScreen',
    headerMode: 'none',
  },
);

const AppContainer = createAppContainer(AppNavigator);

const App = () => {
  return (
    <Provider>
      <AppContainer ref={navigatorRef => Navigation.setTopLevelNavigator(navigatorRef)} />
    </Provider>
  );
};

export default App;
